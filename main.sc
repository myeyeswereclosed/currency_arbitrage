#!/ usr / bin / env -S scala -cli shebang

//> using lib "io.circe::circe-core::0.14.6"
//> using lib "io.circe::circe-parser::0.14.6"
//> using lib "io.circe::circe-generic::0.14.6"
//> using lib "org.typelevel::cats-effect::3.5.2"
//> using lib "com.softwaremill.sttp.client3::cats::3.9.1"
//> using scala 2

import cats.Show
import cats.data.NonEmptyList
import cats.effect.IO
import cats.effect.unsafe.IORuntime
import io.circe._
import io.circe.generic.semiauto._
import io.circe.parser._
import sttp.client3.httpclient.cats.HttpClientCatsBackend
import cats.implicits._
import sttp.client3.{UriContext, basicRequest}

import scala.annotation.tailrec

val url: String = args.length match {
  case 0 => "https://api.swissborg.io/v1/challenge/rates"
  case _ => args(0)
}

val protocolCurrenciesDelimiter: String = args.length match {
  case 0 | 1 => "-"
  case _ => args(1)
}

// For test purpose
implicit private val runtime: IORuntime = IORuntime.global

ExchangeProgram(url, protocolCurrenciesDelimiter).run().unsafeRunSync()

object ExchangeProgram {

  def apply(url: String, protocolCurrenciesDelimiter: String) = new ExchangeProgram(url, protocolCurrenciesDelimiter)

  object Protocol {
    import Domain._

    type ErrorMessage = String

    case class RatesResponse(rates: Map[String, String])

    object RatesResponse {
      implicit val ratesDecoder: Decoder[RatesResponse] = deriveDecoder
    }

    /** Parser and adapter classes could be abstracted for real tasks */
    class RatesResponseParser(data: Either[String, String]) {
      def parse(): Either[ErrorMessage, Map[String, String]] =
        for {
          response <- data
          decodedResponse <- decode[RatesResponse](response).left.map(_.getMessage)
        } yield decodedResponse.rates
    }

    object RatesResponseParser {
      def apply(data: Either[String, String]) = new RatesResponseParser(data)
    }

    class RatesAdapter(data: Map[String, String], delimiter: String) {
      def exchangeRates: Map[CurrencyExchange, Rate] = {
        data.foldLeft(Map.empty[CurrencyExchange, Rate]) {
          case (rates, (currenciesPair, rate)) =>
            val maybeExchangeRate = for {
              exchange <- exchange(currenciesPair)
              rate <- rate.toDoubleOption
            } yield exchange -> rate

            maybeExchangeRate.map(rates + _).getOrElse(rates)
        }
      }

      // Option usage is a simplification
      private def exchange(key: String): Option[CurrencyExchange] =
        key.split(delimiter).map(_.trim) match {
          case Array(from, to) => CurrencyExchange(Currency(from), Currency(to)).some
          case _ => None
        }
    }

    object RatesAdapter {
      def apply(data: Map[String, String], delimiter: String): RatesAdapter = new RatesAdapter(data, delimiter)
    }
  }

  object Domain {

    type Rate = Double

    case class Currency(name: String)
    case class CurrencyExchange(from: Currency, to: Currency)
    case class CurrencyExchangeRate(from: Currency, to: Currency, rate: Rate) {
      /**
       * Used in Bellman-Ford algorithm to find exchange cycles.
       * A small trick to avoid additional structures creation
       */
      val negativeLogRate: Rate = -math.log(rate)
    }

    case class ExchangeArbitrage(rates: NonEmptyList[CurrencyExchangeRate]) {
      private val ratesList = rates.toList

      def value: Rate = ratesList.map(_.rate).product

      // all possible chains with same value
      def exchangeChains: Seq[List[CurrencyExchangeRate]] =
        for {
          i <- ratesList.indices
          (before, after) = ratesList.splitAt(i)
        } yield after ::: before
    }

    case class ExchangeCycle(rates: Set[CurrencyExchangeRate], predecessors: Map[Currency, Currency])

    case class ArbitrageCycle(currencies: NonEmptyList[Currency]) {
      def chainKey: String = currencies.tail.sortBy(_.name).mkString(":")
    }

    case class ExchangeGraph(rates: Set[CurrencyExchangeRate]) {
      val currenciesNum: Int = (rates.map(_.from) ++ rates.map(_.to)).size
    }
  }

  object DomainLogic {
    import Domain._
    import ShowInstances._

    private val maxPossibleRate = Double.PositiveInfinity

    class Arbitrages(exchangeRates: Map[CurrencyExchange, Rate]) {
      def make(): IO[List[ExchangeArbitrage]] = {
        val graph = ExchangeGraph(exchangeRates.map {
          case (exchange, rate) => CurrencyExchangeRate(exchange.from, exchange.to, rate)
        }.toSet)

        for {
          // to show / debug all exchanges
          _ <- IO(println(graph.show))
          arbitrageCycles <- exchangeRates.keySet
            .map(_.from)
            .toList
            .parFlatTraverse(currency => IO(arbitrageCycles(exchangeCycle(currency, graph)).toList))
          // some cycles can have same currencies included, so distinction is applied
          arbitrages <- arbitrageCycles
            .distinctBy(_.chainKey)
            .parFlatTraverse(cycle => IO(makeArbitrage(cycle, exchangeRates).toList))
        } yield arbitrages
      }

      private def makeArbitrage(
         cycle: ArbitrageCycle,
         currenciesExchangeData: Map[CurrencyExchange, Rate]
      ): Option[ExchangeArbitrage] =
        NonEmptyList
          .fromList(
            for {
              (from, to) <- cycle.currencies.sliding2
              rate <- currenciesExchangeData.get(CurrencyExchange(from, to)).map(CurrencyExchangeRate(from, to, _))
            } yield rate
          )
          .map(ExchangeArbitrage)

      private def arbitrageCycles(cycle: ExchangeCycle): Set[ArbitrageCycle] =
        cycle.rates.flatMap(makeArbitrageCycle(_, cycle.predecessors))

      // simplified version
      private def makeArbitrageCycle(
        rate: CurrencyExchangeRate,
        predecessors: Map[Currency, Currency]
      ): Option[ArbitrageCycle] = {
        @tailrec
        def go(
          currency: Currency,
          currentPredecessors: Map[Currency, Currency] = predecessors,
          chain: NonEmptyList[Currency] = NonEmptyList(rate.from, List(rate.to))
        ): Option[ArbitrageCycle] =
          currency match {
            case cur if cur == rate.to => ArbitrageCycle(currency :: chain).some
            case _ =>
              predecessors.get(currency) match {
                case Some(nextCurrency) => go(nextCurrency, currentPredecessors - currency, currency :: chain)
                case _                  => None
              }
          }

        predecessors.get(rate.from).flatMap(go(_))
      }

      // Bellman-Ford algorithm
      private def exchangeCycle(currency: Currency, graph: ExchangeGraph): ExchangeCycle = {
        val (distances, predecessors) =
          relaxation(
            iterationsLeft = graph.currenciesNum - 1,
            graph.rates,
            exchanges = Map.empty[Currency, Rate].withDefaultValue(maxPossibleRate) + (currency -> 0.0)
          )

        ExchangeCycle(
          // identify negative-weight cycle
          rates = graph.rates.filter(rate => distances(rate.from) + rate.negativeLogRate < distances(rate.to)),
          predecessors
        )
      }

      @tailrec
      private def relaxation(
        iterationsLeft: Int,
        exchangeRates: Set[CurrencyExchangeRate],
        exchanges: Map[Currency, Double],
        previous: Map[Currency, Currency] = Map.empty[Currency, Currency]
      ): (Map[Currency, Double], Map[Currency, Currency]) =
        iterationsLeft match {
          case 0 => (exchanges, previous)
          case _ =>
            val (distances, predecessors) =
              exchangeRates.foldLeft((exchanges, previous)) {
                case ((distances, predecessors), exchangeRate) =>
                  exchanges(exchangeRate.from) + exchangeRate.negativeLogRate match {
                    case nextExchange if nextExchange < exchanges(exchangeRate.to) =>
                      (
                        distances + (exchangeRate.to -> nextExchange),
                        predecessors + (exchangeRate.to -> exchangeRate.from)
                      )
                    case _ => (distances, predecessors)
                  }
              }

            relaxation(iterationsLeft - 1, exchangeRates, distances, predecessors)
        }
    }

    object Arbitrages {
      def apply(exchangeRates: Map[CurrencyExchange, Rate]) = new Arbitrages(exchangeRates)
    }
  }

  object ShowInstances {
    import Domain._

    implicit val exchangeRateShow: Show[CurrencyExchangeRate] =
      Show.show[CurrencyExchangeRate](rate => s"${rate.from.name} -> ${rate.to.name} [${rate.rate}]")

    implicit val exchangeGraphShow: Show[ExchangeGraph] =
      Show.show[ExchangeGraph] { graph =>
        s"""
           |Exchange rates:
           |${graph.rates.toList.sortBy(rate => (rate.from.name, rate.to.name)).map(_.show).mkString("\r\n")}
           |""".stripMargin
      }

    implicit val arbitrageShow: Show[ExchangeArbitrage] =
      Show.show[ExchangeArbitrage] { arbitrage =>
        s"""
           |Arbitrage value=[${arbitrage.value}]:
           |${arbitrage.exchangeChains.map(_.show).mkString("\r\n")}
           |""".stripMargin
      }

    implicit val arbitrageChainShow: Show[List[CurrencyExchangeRate]] =
      Show.show[List[CurrencyExchangeRate]] { chain =>
        s"${(chain :+ chain.head).map(_.from.name).mkString(" -> ")} [${chain.map(_.rate).mkString("*")}]"
      }
  }
}

class ExchangeProgram(url: String, protocolCurrenciesDelimiter: String) {

  import ExchangeProgram.Protocol._
  import ExchangeProgram.ShowInstances._
  import ExchangeProgram.DomainLogic._

  def run(): IO[Unit] =
    HttpClientCatsBackend
      .resource[IO]()
      .use { client =>
        for {
          _ <- IO(println(s"Sending request to $url..."))
          response <- client.send(basicRequest.get(uri"$url"))
          _ <- IO(println(s"Parsing response..."))
          result <- RatesResponseParser(response.body).parse().flatTraverse {
            data =>
              val exchangeRates = RatesAdapter(data, protocolCurrenciesDelimiter).exchangeRates
              Arbitrages(exchangeRates).make().map(_.asRight[ErrorMessage])
          }
        } yield
          result.fold(
            error => println(s"Error occurred: $error"),
            arbitrages =>
              arbitrages.sortBy(-_.value) match {
                case Nil => println("No arbitrages found")
                case h :: tail =>
                  println(s"""
                    |The best arbitrages found:
                    |${h.show}
                    |Other possible arbitrages found:
                    |${tail.map(_.show).mkString}
                    |""".stripMargin
                  )
            }
          )
      }
}

